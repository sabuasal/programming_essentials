
#include <cmath>
#include <cstdio>
#include <cassert>
#include <vector>
#include <map>
#include <iostream>
#include <algorithm>
using namespace std;
map<unsigned int, unsigned long long> vals_map;
using pair_type = decltype(vals_map)::value_type;

unsigned long long max = 0;
void insert_range(unsigned int a, unsigned int b, unsigned long k) {
  auto it = vals_map.lower_bound(a - 1);
  auto val = it->second;
  // Same element found?
  if (it->first == a -1) {
    // Do nothing, since in our implemetation the value for a range is 
    // the value of the lower_bound of the range!!
    // advacne it to point to the value for that (a)element
    it++;
    val = it->second;
  } else {
  // Not found, need lower bound!
    // vals_map[a - 1] = val;
    vals_map.insert (it, std::pair<unsigned int,unsigned long long>(a-1, val));
  }

  if (a == b) {
     vals_map[a] = val + k;
     // vals_map.insert (it, std::pair<unsigned int,unsigned long long>(a, val+k));
     return;
  }
  
  // loop till we get to the upper bound for b and update 
  // the map in the process.
  for(; it->first < b; it++) {
    it->second += k; // increment everything in between
  }

  // If it is B, just quit
  if (it->first == b) {
    it->second += k;
    return;
  }

  val = it->second;
  vals_map.insert (it, std::pair<unsigned int,unsigned long long>(b, val+k));
  //vals_map[b] = val + k;
  return;
  //}
  //return; 
}

int main() {
  long int N, M, a, b;
  long int sum =0;
  long int k;
  cin >> N >> M;
  long int* prefix_sum = new long int[N +1];
 
  // fill (prefix_sum, prefix_sum + sizeof(prefix_sum)*(N + 1), 0);
  for (long int n =0; n <= N +1; n++) {
    prefix_sum[n] = 0;
  }

  for (long int m =0; m < M; m++) {
      cin >> a >> b >> k;
      prefix_sum[a - 1] += k;
      prefix_sum[b]     -= k;
  }

  // Build the prefix sum:
  sum = prefix_sum[0];
  for (long int n =1; n <= N +1; n++) {
    prefix_sum[n] += prefix_sum[n-1];
    // cout << prefix_sum[n] << ",";
    if (sum < prefix_sum[n])
      sum = prefix_sum[n];
  }

  cout << sum << "\n";
  // delete[] prefix_sum;
}

int main_old() {
    /* Enter eyour code here. Read input from STDIN. Print output to STDOUT */   
    unsigned int N, M, a, b;
    unsigned long k;
    cin >> N >> M;
    vector<unsigned long long> vals;
    vals.assign(N,0);
    //fill(vals.begin(), vals.end(), 0);

    vals_map[1] = 0; 
    vals_map[N] = 0;

    for (long int m=0; m < M; m++) {
      cin >> a >> b >>k;
      // std::for_each(vals.begin() + a -1, vals.begin() + b , [k]( unsigned long long  &n){ n+=k; });
      insert_range(a, b, k);
    }
    
    // cout << *std::max_element(vals.begin(), vals.end());
    // cout << std::max(vals_map, [](map<unsigned int, unsigned long long>::iterator ii){ return ii->second});
    auto pr = std::max_element
    (
      begin(vals_map), end(vals_map),
        [] (const pair_type & p1, const pair_type & p2) {
	        return p1.second < p2.second;
     }
    );
    cout << pr->second << "\n";
    return 0;
}

