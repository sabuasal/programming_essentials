#include <iostream> 
#include <vector> 
using namespace std;

//void incr(int num) {
//  cout << "Incremnet by value\n";
//  num++;
//}

void incr(int& num) {
  cout << "Incremnet by lvalue ref\n";
  num++;
}

void incr_(int&& num) {
  cout << "Incremnet by rvalue ref with incr_\n";
  num++;
}


void incr(int&& num) {
  cout << "Incremnet by rvalue value\n";
  num++;
}

int main() {
  int a = 9;
  int b = 9;
  vector<int> numbers;

  cout << "incr(a);\n";
  incr(a);

  cout << "incr(a + b);\n";
  incr(a + b);

  cout << "incr(move(a));\n";

  incr(move(a));

  numbers.push_back(1);
  numbers.push_back(1);
  numbers.push_back(1);
  numbers.push_back(1);
  numbers.push_back(1);
  for (auto& nn: numbers) {
    incr(nn);
    cout << nn << endl;
  }
  for (auto& nn: numbers) {
    cout << nn << endl;
  }

  auto ff = incr_;
  ff(5);
}

