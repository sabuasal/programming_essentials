#include <utility>
#include <cmath>


template <class T>
class sorter {

  public:
    sorter(T  container):array((container)){}
    void sort() {
      merge_sort(array.begin(), array.end(), array.size());
    }
    void print() {
      for (auto v:array)
        cout << v << "\t";
      cout << endl;
    }

  private:
    T array;
    template <class iterator>
    void merge(iterator start, iterator mid, iterator end) {
      T buf1(start, mid); 
      T buf2(mid, end);

      auto start1 = buf1.begin();
      auto start2 = buf2.begin();
      
      while (start1 != buf1.end() && start2 != buf2.end()) {
        if ((*start1) < (*start2)) {
          *start = *start1;
          start1++;
        } else {
          *start = *start2;
          start2++;
        }
        start++;
      }

      //Now check if the buffers are empty!
      while (start1 != buf1.end()) {
        *start = *start1;
        start1++;
        start++;
      }
      while (start2 != buf2.end()) {
        *start = *start2;
        start2++;
        start++;
      }
    }

    template <class iterator, class sz>
    void merge_sort(iterator start, iterator end, sz size) {
      // cout << "printing from merge sorting stuff yooooo with length: " << size << endl;
      if (size == 1) {
        return;
      } else if (size == 2) {
        // sort things
        auto second = start;
        second++;
        if ((*start) > (*second)) {
          iter_swap(start, second);
        }
      }
      auto loop = start; 
     // for (;loop != end; loop++) {
     //   cout << *loop << " "; 
     // }

      auto mid      = start;
      advance(mid,  ceil(1.0*size/2)); // For odd sizes, let the first be the bigger list
      merge_sort(start, mid, ceil(1.0*size/2));
      merge_sort(mid  , end,floor(1.0*size/2));
      merge(start, mid, end);
    }

};
