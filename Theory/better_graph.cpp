#include <assert.h> 
#include <iostream> 
#include <vector> 
#include <queue> 
#include <unordered_set> 
#include <set> 
#include <unordered_map> 
#include <string>
#include <stack>

using namespace std;
template <typename T>
class Node {
  public:
    T PayLoad;
    unordered_map<Node*, int> Nebs;
    Node(T L) : PayLoad(L) {}
    void addNeighbour(Node* y, int weight) {
      if (Nebs.count(y))
        cerr << "Edge already here, updating\n";
      Nebs[y] = weight; 
    }
    friend ostream& operator<<(ostream& os, const Node& obj) {
      os << "Node: " << obj.PayLoad << "\n\t";
      for (auto& it : obj.Nebs)
        os <<"\t(" <<  it.first->PayLoad << ", " << it.second << ")";
      return os;
    }
};

template <typename T>
class Graph {
  public:
    // unordered_set<Node*> Nodes;
    set<Node<T>*> Nodes;
    string Name;
  public:
    Graph (string name): Name(name){
      //cout << "Building a " << Name << " graph\n";
    };
    Node<T>* addNode(T PayLoad) {
      auto* n =  new Node<T>(PayLoad);
      Nodes.insert(n);
      return n;      
    }
   void addEdge(Node<T>* x, Node<T>* y, int weight) {
     if (!(Nodes.count(x) && Nodes.count(y))) {
       cerr << "Both nodes should exist in the Graph before adding the edge\n";
       return;
     }
     x->addNeighbour(y, weight);  
   }
   friend ostream& operator<<(ostream& os, const Graph<T>& obj) {
      os << "Graph: " << obj.Name << "\n";
      for (auto& it : obj.Nodes)
        os <<"\t" << *it <<"\n";
      return os;
   }
   void fill(vector< Node<T> * >& v) {
      for (auto& it : Nodes)
         v.push_back(it);
   }

  ~Graph() {
     for (auto& it : Nodes)  
       delete it;
   }
};

void test_cities() {
  Graph<string> G("Cities");
  auto *zar = G.addNode("Zarqa");
  auto *amm = G.addNode("Amman");
  auto *irb = G.addNode("Irbid");
  G.addEdge(zar, amm, 6);
  G.addEdge(irb, amm, 2);
  G.addEdge(amm, irb, 5);
  G.addEdge(zar, irb, 3);
  cout << G;
}

template <typename T>
void BFS(Graph<T>& G , Node<T>* N1) {
 queue< Node<T>* > Q;
 Q.push(N1);
 while (!Q.empty()) {
   Node<T>* N = Q.front();
   cout << N->PayLoad;
   for (auto it : N->Nebs)
     Q.push(it.first);
   Q.pop();
 }
  
 cout << "\n";
}

template <typename T>
void BfsByLevel(Graph<T>& G , Node<T>* N1) {
 queue< Node<T>* > Q;
 Q.push(N1);
 Q.push(nullptr);

 while (!Q.empty()) {
   Node<T>* N = Q.front();
   while (nullptr != N) {
     Q.pop();
     cout << N->PayLoad;
     for (auto it : N->Nebs)
       Q.push(it.first);
     N = Q.front();
  }
  Q.pop(); // remove the null .
  if (!Q.empty()) // break the next level (if any) with nullptr.
    Q.push(nullptr);
  cout << "\n";
 }
}



void testNums() {
  Graph<int> G("Numbers");
  
  Node<int>* N1 = G.addNode(1);
  auto* N2 = G.addNode(2);
  auto* N3 = G.addNode(3);
  auto* N4 = G.addNode(4);
  auto* N5 = G.addNode(5);
  auto* N6 = G.addNode(6);
  auto* N7 = G.addNode(7);
  // auto* N8 = G.addNode(8);

  G.addEdge(N1, N2, 0);
  G.addEdge(N1, N3, 0);

  G.addEdge(N2, N4, 0);
  G.addEdge(N2, N5, 0);

  G.addEdge(N3, N6, 0);
  G.addEdge(N3, N7, 0);
  // cout << G;
  
  // BFS(G,N1);  
  BfsByLevel(G,N1);  
  return;
  // queue< decltype(N1) > Q;
  queue< Node<int>* > Q;
  Q.push(N1);
  while (!Q.empty()) {
    auto* N = Q.front();
    Q.pop();
    cout << N->PayLoad;
    for (auto& it : N->Nebs)
      Q.push(it.first);
  }
  
  cout << "\n";
}

template <typename T>
void TopSort(Graph<T>& G, vector<Node<T>*>& Order) {
  vector<Node<T>*> WL;
  G.fill(WL);

  set<Node<T>*> visiting, visited; 

  while (!WL.empty()) {
    // Find a root node to start tracersing DFS with:
    auto* Root = WL.front();
    WL.erase(WL.begin());

    //auto* Root = WL[(WL.size()/2)];
    if (visited.count(Root)) { // find another root, this is already vsited!
      continue;
    }
    cout << "Root: " << Root->PayLoad << "\n";
    stack<Node<T>*> S; S.push(Root);
    while (!S.empty()) {
      auto N = S.top();
      cout <<"top: " << N->PayLoad << "\n";
      visiting.insert(N);
      bool found_neb = false;

      for (auto& it : N->Nebs) {
        assert (!visiting.count(it.first) && "Cycle detected\n");
        if (0 == visited.count(it.first)) {
          cout << "Adding " << it.first->PayLoad << " to stack\n";
          S.push(it.first);
          found_neb = true;
          break;
        }
      }
    
      if (!found_neb) {
        // No neighburs found, pop this element.
        visited.insert(S.top());
        Order.push_back(S.top());
        visiting.erase(S.top());
        cout << "Poping: " << S.top()->PayLoad << "\n";
        S.pop();
      }
    } // S.empty()
  } // WL.empty()
}

void testTopSort() {
  Graph<string> G("Tasks");
  auto* NA = G.addNode("A");
  auto* NB = G.addNode("B");
  auto* NC = G.addNode("C");
  auto* ND = G.addNode("D");
  auto* NE = G.addNode("E");
  auto* NF = G.addNode("F");

  G.addEdge(NA, NB, 0);
  G.addEdge(NB, NC, 0);
  G.addEdge(NC, NE, 0);
  G.addEdge(NC, NF, 0);
  G.addEdge(ND, NE, 0);  
  G.addEdge(ND, NB, 0);  

  G.addEdge(ND, NA, 0);  
  G.addEdge(NE, NF, 0);
 // G.addEdge(NF, NE, 0);

  vector<Node<string>*> Order;
  TopSort(G, Order);
  while (!Order.empty()) {
    cout << Order.back()->PayLoad << " "; Order.pop_back();
  }
  cout << "\n";
}

int main() {
  testTopSort();
  return 0;
}
