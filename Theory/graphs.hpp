#include "structures.hpp"
#include <vector>
#include <assert.h>
#include <queue>
#include <stack>
#include <limits>
#include <algorithm>
#include <functional>
#include <utility>
#include <utility>      // std::pair


using namespace std;

template <class T>
class graph {

  enum edge_type {
    Tree =0, 
    Back, 
    Cross
  };

  class node_edge {
    public:
      T label;
      edge_type type;
      int weight;
      size_t end_node; // Since we are doing an adjecancy list we don't need a start node, 
                       // the start is the node at the entry.
      node_edge(int weight, size_t end_node):weight(weight), end_node(end_node), type(edge_type::Tree) {
      }
  };
  typedef linked_list<graph::node_edge*> list_of_nodes;
  typedef typename linked_list<graph::node_edge*>::iterator list_of_nodes_iter;

  private:
    vector<linked_list<node_edge*>> vertices;
    vector<bool>   visited;
    vector<short>  discover_time;
    vector<short>  finish_time;
    vector<int>  distance;
    vector<size_t> parent;
    size_t vertex_count;
    size_t edge_cout;
    bool traversed;

  public:
    graph(size_t vertex_count):vertex_count(vertex_count),traversed(false) {
      for (size_t idx=0; idx < vertex_count; idx++) {
//        auto tmp = new linked_list<node_edge*>();
//        vertices.push_back(tmp);
        vertices.push_back(*(new linked_list<node_edge*>()));
      }
      // Initialize the visieted vector to false.
      visited.resize(vertices.size(), false); 
      discover_time.resize(vertices.size(), -1); 
      finish_time.resize(vertices.size(), -1);
      parent.resize(vertices.size(), -1);
      distance.resize(vertices.size(), numeric_limits<int>::max());
    }

   friend ostream& operator<<(ostream& out, graph& right)  {
     if (!right.traversed) { 
      for (size_t idx=0; idx < right.vertex_count; idx++) {
         out << "Vertex: " << idx << ":" << endl;
         //for (auto b = right.vertices[idx].begin(); b != right.vertices[idx]._end(); b.p = b.p->next) {
         for (list_of_nodes_iter  b = right.vertices[idx].begin(); b != right.vertices[idx].end(); ++b) {
           out << "\tNeighbor: " << b->end_node << endl;
         }
      }
    } else {
      out << "vetex:\t";
      for (size_t idx=0; idx < right.vertex_count; idx++) {
        out << idx << "\t";
      }
      out << "\n";

      out << "start:\t";
      for (size_t idx=0; idx < right.vertex_count; idx++) {
        out << right.discover_time[idx] << "\t";
      }
      out << "\n";

      out << "end:\t";
      for (size_t idx=0; idx < right.vertex_count; idx++) {
        out << right.finish_time[idx] << "\t";
      }
      out << "\n";
      out << "dist:\t";
      for (size_t idx=0; idx < right.vertex_count; idx++) {
        out << right.distance[idx] << "\t";
      }
      out << "\n";
    }
      return out;
   }

    ~graph() {
    }

    void insert_edge(size_t node_x, size_t node_y, int weight=0) {
      if (node_x >= vertices.size()) {
        cerr << "Error, can't insert node: " << node_x << " graph only has " << 
          vertices.size() << " nodes"<< endl;
        return;
      }

      if (node_y>= vertices.size()) {
        cerr << "Error, can't insert node: " << node_y << " graph only has " << 
          vertices.size() << " nodes"<< endl;
        return;
      }

      // Create edge and insert it.
      auto tmp = new node_edge(weight, node_y);
      //vertices[node_x]->insert(tmp);
      vertices[node_x].insert(tmp);
    }

    void BFS(size_t start=0) {
      cout << "BFSinggg with start idx: " << start << endl;
      // fill in wueue wit the start vertex
      // pop a vertex from the queue
      // for each vertex poppoed, process the vertex insert it in the queue
      queue<size_t> visit_q;
      visit_q.push(start); 
      // Initialize all the nodes to not viisted
      fill (visited.begin(), visited.end(), false);

      while (!visit_q.empty()) {
        auto ele = visit_q.front(); 
        visit_q.pop(); 
        cout << " visited element : " << ele << endl;
        for (auto v = vertices[ele].begin(); v != vertices[ele].end(); ++v) {
          // cout << "Adding: " << v->end_node << endl;
          if (!visited[v->end_node]) {
            visit_q.push(v->end_node);
            visited[v->end_node] = true;
          }
        }
      }
    }

    void pre_process_vertex(size_t v, size_t& time, size_t parent_) {
       // cout << "Processing: " << v << " at " << time << endl;
       visited[v]       =  true; 
       discover_time[v] = time++;
       parent[v]        = parent_;
       // cout << " pre preocess Visited element : " << v << endl;
    }

    void post_process_vertex(size_t v, size_t& time) {
        cout << "popped " << v << " at " << time << endl;
       finish_time[v] = time++;
    }

    void pre_process_edge(size_t start_node, node_edge* edge) {

    }

    void Dijk(size_t start =0){ 
      vector<pair<size_t,int>>  vertex_distance; // to apply Dijkestras algoruthm.
      // Initial distance to inf and the father to nil except for the root
      fill (parent.begin(), parent.end(), -1);
      fill (visited.begin(), visited.end(), false);
      fill (distance.begin(), distance.end(), numeric_limits<int>::max());
      vertex_distance.push_back(make_pair(start, 0));
	class ComparePair {
	  public:
    	    bool operator()(pair<int,int> n1,pair<int,int> n2) {
            return n1.second>n2.second;
    	  }
	};

      auto compareFunc = [](pair<int,int> a, pair<int, int> b) { return a.second > b.second; };
      auto addFunc = [](int  a, int b) { return a + b; };
      // priority_queue<pair<size_t, int>, vector<pair<size_t, int>>, ComparePair > Qu;
      priority_queue<pair<size_t, int>, vector<pair<size_t, int>>, decltype(compareFunc) > Qu(compareFunc);
      auto hh = addFunc;
      cout << hh(9,9) << endl;
      Qu.push(make_pair(start,0));
      distance[start] = 0;
      while (!Qu.empty()) {
        auto tt = Qu.top();
        Qu.pop();
        auto src = tt.first;
        // Now go over the edges and relax the vertices attached ot the end and enqueue them!
        for (auto v = vertices[src].begin(); v != vertices[src].end(); ++v) {
          // Relax the vertex then enqueue it if it was not visited before!
          if (distance[v->end_node]  > distance[src] + v->weight) {
            distance[v->end_node] = distance[src] + v->weight;
          }

          // Enqueue vertex if it was not visited before!
          if (!visited[v->end_node]) { 
            Qu.push(make_pair(v->end_node, distance[v->end_node]));
            visited[v->end_node] = true;
          }
        }
      }
    }

    void DFS(size_t start=0) {
      cout << "DFSinnnngg with start index: " << start << endl;
      stack<size_t> visit_order;
      size_t time = 0;
      visit_order.push(start);
      pre_process_vertex(start, time, -1);
      while (!visit_order.empty()) {
        auto ele = visit_order.top();
        bool neb_found = false;
        for (auto v = vertices[ele].begin(); v != vertices[ele].end(); ++v) {
          if (!visited[v->end_node]) {
            // tree edge
            v->type = Tree;
            pre_process_vertex(v->end_node, time, ele);
            visit_order.push(v->end_node);
            neb_found = true;
            break; // once we find an unvisited neighbor, we pus it and ignore other neighbors for now!!
          } else {
            if (finish_time[v->end_node] != -1) {
              // if the end nod has a finish time, it is a cross edge
              v->type = Cross;
              // cout << "Found a cycle from: " << finish_time[v->end_node] << " to " << finish_time[ele] << endl;
            } else {
              // else, it is a back edge (cycle yoo)
              cout << "Found a cycle from: " << v->end_node << " to " << ele << endl;
              v->type = Back;
            }
          }
        }
        if (!neb_found) {
          // no children found, start back tracking
          visit_order.pop(); // remove the element
          post_process_vertex(ele, time);
        }
      }
      traversed  = true;
    }
};


