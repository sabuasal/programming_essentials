#include "structures.hpp" 
#include "algorithms.hpp"
#include "graphs.hpp"
#include <random>
#include <list>

void test_linked_list() {
 linked_list<int> ll;
 linked_list<int> empty;
 ll.insert(8);  
 ll.insert(9);  
 ll.insert(10);  
 ll.insert(11);  
 ll.insert(12);  
 cout << ll;
 cout << empty;
 cout << "After deleting the elemnets from ll\n";
 ll.remove(8);
 cout << ll;
 ll.remove(10);
 cout << ll;
 ll.remove(9);
 cout << ll;
 ll.remove(12);
 cout << ll;
 cout << "removing 11\n"; 
 ll.remove(11);
 cout << ll;

 binary_tree<int> tt; 
 tt.insert(50);
 tt.insert(20);
 tt.insert(70);
 tt.insert(10);
 tt.insert(5);
 tt.insert(30);
 tt.insert(60);
 tt.insert(80);
 tt.insert(52);
 tt.insert(65);
 tt.insert(75);
 tt.insert(90);

 cout << "original:\n";
 cout << tt;
 cout << "After removing 70:\n";
 tt.remove(70);
 cout << tt;
 cout << "After removing 50:\n";
 tt.remove(50);
 cout << tt;


}

void test_heap() {
  heap<double> hh;

  random_device rd;
  default_random_engine e1(rd());
  uniform_int_distribution<int> uniform_dist(1,200);
  for (auto idx =0; idx < 15; idx++) {
    hh.insert(uniform_dist(e1));
  }
  cout << hh;
  while (hh.is_full()) {
    // cout << hh.get_top() << " ";
    auto rr = hh.get_top();
    cout << hh; 
  }
  cout << endl;
}

class car {
  private:
   int price;
 public:
   car(int pp):price(pp) {}
   friend bool operator< (car& left, car& right) {
     return (left.price < right.price);
   }

   friend bool operator> (car& left, car& right) {
     return (left.price > right.price);
   }

   friend bool operator== (car& left, car& right) {
     return (left.price == right.price);
   }

 friend ostream& operator<< (ostream& out, car& right) {
     out << "car:" << right.price;
     return out;
  }
};


void test_sorter() {
   //vector<int> cc;
   list<int> cc;
   vector<car> cars;
   random_device rd;
   default_random_engine e1(rd());
   uniform_int_distribution<int> uniform_dist(1,200);
   for (auto idx =0; idx < 17; idx++) {
     cc.push_back(uniform_dist(e1));
     cars.push_back(car(uniform_dist(e1)));
   }
   cout << "Before constructor the length is: " << cc.size() << " " << cars.size() << endl;
   sorter<list<int>> ss(move(cc)); // This constructor uses move semantics to copy the memory in the vectoe
   sorter<vector<car>> sscars(move(cars)); // This constructor uses move semantics to copy the memory in the vectoe
   cout << "After constructor the length is: " << cc.size()  << " " << cars.size() << endl;
   cout << "Not sorted:\t";
   ss.print();
   ss.sort();
   sscars.sort();
   cout << "Sorted:\t\t";
   ss.print();
   sscars.print();
}
void test_graph() {
   graph<string> gg(6);
   gg.insert_edge(0,1, 1);
   gg.insert_edge(0,3, 1);
   gg.insert_edge(1,2, 1);
   gg.insert_edge(1,3, 1);
   gg.insert_edge(3,5, 1);
   gg.insert_edge(3,4);
   gg.insert_edge(5,0);
   cout << gg;
//   gg.BFS(0);
   cout << "After traversing the graph" << endl;
   gg.DFS(0);
   cout << gg;
   cout << "After running Dijk" << endl;
   gg.Dijk(0);
   cout << gg;
}

int main() {
  test_graph();
  return 0; 
}
