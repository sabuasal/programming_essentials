#ifndef _STRUCTURES_HPP_
#define _STRUCTURES_HPP_

#include <cstddef>
#include <iostream>
#include <vector>

using namespace std;

// Linked List
// Insert, Search, delete
// Iterators for Traversal

#ifdef DEBUG
  #define LOG cout
#else
  #define LOG cout
#endif

template<class T> 
class linked_list {
  public:

    class entry {
      private:
      public:
        T value;
        entry* next;
        entry* back;
        entry(T value, entry* next = nullptr, entry* back = nullptr) {
          this->value = value;
          this->next = next;
          this->back = back;
        }
    };

    class iterator {
      private:
      public:
        entry* p;
        
      public:
        iterator(entry* p):p(p) {}

        iterator& operator++() { //pre increent
          p = p->next;
          return *this;
        }

        iterator& operator++(int) { //post incremen
          auto ret = *this;
          p = p->next;
          return ret;
        }

        T operator->() {
          return p->value;
        }

        entry* operator*()  {
          return p;
        }

       void  increment() {
          p = p->next;
       }
 //        friend T operator->() {
 //         return p->value;
 //       }

        iterator& operator--() {
          p = p->back();
          return *this;
        }



        friend bool operator==(iterator& left, iterator& right) {
          return (right->p == left->p);
        }

        friend bool operator!=(const iterator& left, const iterator& right) {
          return !(right.p == left.p);
        }
//        friend iterator& operator=(T arg) {
//          this->p = arg.p;
//          return *this;
//        }
    };


    iterator begin() {
      return iterator(root);
    }

    iterator end() {
        return iterator(nullptr);
    }

    linked_list() {
      root = last = nullptr;    
    }

    void insert(T value) {
      if (!root) {
        root = new entry(value); 
        last = root;
      } else {
        last->next = new entry(value, nullptr, last); //Insert at end.
        last = last->next;
      }
      // cout << "root: " << root->value << " last: " << last->value << "\n";
    };

    const entry*  search(T value) {
      entry* v = root;
      while (v != nullptr && v->value != value)
        v = v->next;
      return v;
    }

    void remove(T value) {
      auto node = search(value);
      if (!node) {
        cout << "Node: " << value << " not found in list\n";
        return; // Nothig to do here, the value doesn't exist!
      }

      if (node->next != nullptr && node->back != nullptr) {
        // This is a middle node.
        node->back->next = node->next;
        delete node; // release memory
      } else if(node->next == nullptr && node->back == nullptr) {
        // An only node
        root = last = nullptr;
        delete node;
      }
      else if(node->back == nullptr) {
        // This is a root node
        root = root->next;
        root->back = nullptr;
        delete node;
      } else if(node->next == nullptr) {
        // This is the last pointer
        node->back->next = nullptr;
        last = node->back;
        last->next = nullptr;
        delete node;
      }
      
//      cout << "After deleting: " << value <<  " root node is: "<< root->value << "\n";
    }

   const entry* get_root() const {
      return root;
    }

  private:
    entry* root;
    entry* last;
    unsigned int height;    
};

// Binary Tree 

template<class T> 
class binary_tree {
  public:
    class entry {
      public:
        T value;
        entry* left;
        entry* right;
        entry* parent;
        entry(T value, entry* parent = nullptr, entry* left = nullptr, entry* right = nullptr) {
          this->value = value;
          this->left  = left;
          this->right = right;
          this->parent= parent;
        }

        bool is_left_child() {
          if(!parent)
            return false; // doesn't have a parent!
          if (value < parent->value)
            return true;
          return false;
        }

        bool is_right_child() {
          return (!is_left_child());
        }

        bool is_leaf() {
          return ((left == nullptr) && (right == nullptr));
        }

        bool is_root() {
          return (parent == nullptr);
        }
    };

    binary_tree() {
      root  = nullptr;    
    }

    void find_max() {
    }

    void find_min() {
    }

   // non recursive inset  
    void insert(T value) {
      if (!root) {
        // We reached a case where the leaf is not allocated, allocate it and save it.
        root = new entry(value); // esnding a ointer to the pointer in the parent here
        return;
      }

      auto insert_at = root;
      while(true) {
        if (value < insert_at->value) {
          if (insert_at->left == nullptr) {
            insert_at->left = new entry(value, insert_at);
            break;
          } else {
            insert_at = insert_at->left;
            continue;
          }
        } else {
           if (insert_at->right == nullptr) {
            insert_at->right = new entry(value, insert_at);
            break;
          } else {
            insert_at = insert_at->right;
            continue;
          }
        }
      }
    }


    void re_insert(entry* subtree) {
      if (subtree == nullptr) 
        return;

      auto insert_at = root;
      while(true) {
        if (subtree->value < insert_at->value) {
          if (insert_at->left == nullptr) {
            insert_at->left = subtree;
            subtree->parent = insert_at;
            cout << "re-insreted " << subtree->value  << " left of " << insert_at->value << "\n";
            break;
          } else {
            insert_at = insert_at->left;
            continue;
          }
        } else {
           if (insert_at->right == nullptr) {
            insert_at->right = subtree;
            subtree->parent  = insert_at;
            cout << "re-insreted " << subtree->value << " right of " << insert_at->value << "\n";
            break;
          } else {
            insert_at = insert_at->right;
            continue;
          }
        }
      }
    }

    entry* visit_search(T value, entry* search_at) {
      entry* res = nullptr;
      while(search_at) {
        if (search_at->value == value) {
          cout << "found " << value << endl;
          return search_at;
        } else {
          if (value < search_at->value)
            return visit_search(value, search_at->left);
          else 
            return visit_search(value, search_at->right);
        }
      }
      cout << value << " not in tree\n";
      return nullptr;
    }

    entry*  search(T value) {
      if (!root) {
        cout << "Tree is empty!\n";
        return nullptr;
      } else {
        return visit_search(value, root);
      }
    }

    void visit(ostream& out, entry* v) const {
      if (v) {
        out << v->value << " ";
        visit(out, v->left);
        visit(out, v->right);
      } else {
        return;
      }
    }

   void remove(T value) { 
     auto node = search(value);
     if (!node) {
       return; // nothing to do, value doesn't exist 
     }


     if (node->is_root()) { // coud also be (node==root)
       if (node->is_leaf()) {
         // it is an only node tree!
         root = nullptr;
       } else {
         // If it has children, pick a child to be 
         // a root and re-insert the other subtree.
         if (node->left) {
           node->left->parent = nullptr; // the left child is now root. roots has no parents.
           root = node->left;
           if (node->right) {  // re insert the right node, if it exists
             node->right->parent = nullptr; // parents are nulled, will be refilled inside re-insert.
             re_insert(node->right);
           }
         } else {
           // if left is empty, right must be there!
           node->right->parent = nullptr;
           root = node->right;
           if (node->left) {
             node->left->parent = nullptr;
             re_insert(node->left);
           }
         }
       }
       // delete the root and it is not a leaf 
       // delete a root has one child
       // delete a root has two children
     } else { // not a root
       // simple leaf node.
       if (node->is_leaf()) {
         // Just a leaf node.
         // The parent to not point to it anymore 
         if (node->is_left_child()) {
           node->parent->left = nullptr;
         } else {
           node->parent->right = nullptr;
         }
      } else {  
        // The node has sub node that might in turn also have sub nodes.
        // if the node is left node, short circuit its left child to it's parent and Re-insert the right tree (if it has any).
          if (node->is_left_child()) {
            node->parent->left  = node->left; // If the left is null we are still ok
            // now re-insert the right subtree
            node->right->parent = nullptr;
            if (node->left)
              re_insert(node->right); 
          } 
            // if the node is right node, short circuit  the right child to it's parent and Reinert the left tree (if it has any).
          else { 
            node->parent->right = node->right;
            node->left->parent  = nullptr;
            if (node->left)
              re_insert(node->left);
          }
      } // Not a leaf and has a parent (not root).
    }
   
     delete node;
   }

   void traverse(ostream& out) const {
     if (root) {
      visit(out, root);
      out << "\n";
     } else {
       out << "This is an empty tree!!\n";
     }
   }

  private:
    entry* root;
};

template <class T>
ostream& operator<<(ostream& out, const linked_list<T>& l) {
  auto v = l.get_root();
  if (!v) {
    out << "The list is empty\n";
  } else {
    while (v != nullptr) {
      out << v->value << " ";
      v = v-> next;
    }
    out << "\n";
  }
  return out;
}

template <class T>
ostream& operator<<(ostream& out, const binary_tree<T>& l) {
  l.traverse(out);
  return out;
}
// Heap
// Make heap
// insert
// get_top max or min!
// sort the heap


template <class T> 
class heap {
  public:
    vector<T> array;
    heap() {}

    void insert(T value){
      array.push_back(value);
      bubble_up();
    }

    bool is_full() {
      return (array.size() > 0);
    }
    T get_top() {
      // return the top
      // move the last element to top
      // bubble_down
      auto ret = array[0];
      array[0] = array[array.size() - 1];
      array.erase(array.end() - 1); // end iterator sets at one spot  beyond the last element.
      bubble_down();
      return ret;
    }

    void print(ostream& out) const {
      for(int i=0; i<array.size(); ++i)
        out << array[i] << ' '; 
      out << endl;
    }

  private:

    void bubble_down(size_t start_idx = 0) { 
      // start bubling down from 0 if no start index is given for bubbling
      auto min_idx = start_idx; // Need to find the minmum amoung the parent
                                //  that is about to get bubbled down and the two childern.
      size_t child_1 = 0;
      size_t child_2 = 0;

      while (get_child(start_idx, child_1, child_2)) {
        if (array[child_1] < array[min_idx])
          min_idx = child_1;
        if (array[child_2] < array[min_idx])
          min_idx = child_2;
        if (min_idx != start_idx) { // Not the minmum, bubble down 
          iter_swap(array.begin() + min_idx, array.begin() + start_idx);
          start_idx = min_idx; // Now bubble again
        } else {
          break;
        }
      }      
    }

    size_t get_parent(size_t idx) {
      if (idx == 0) { 
        return -1;
      }
      return ((idx-1)/2);
    }

    bool get_child(size_t parent, size_t& first, size_t& second) {
      first  = 2*parent +1;
      if (first >= array.size())
        return false;
      second = 2*parent +2;
      if (second >= array.size()) // for cases when there is only one child, set the second to value of the first.
        second = first;

      return true;
    }
 
    void bubble_up() {
      auto idx_last = array.size() - 1;
      auto  parent  = get_parent(idx_last);
      while (parent != -1) {
        if (array[idx_last] < array[parent]) {
          // swap these two values
          // auto tmp = array[parent];
          iter_swap(array.begin() + idx_last, array.begin() + parent);
          idx_last = parent;
          parent  = get_parent(idx_last);
        } else {
          break; 
        }
      }
    }

  friend ostream& operator<<(ostream& out, const heap<T>& l) {
    l.print(out);
    return out;
  }
};


// RedBlack tree (Balanced Binary Tree).
// chapter 12 !!

   //   if (!insert_at) { 
   //     // this is the first node inserted, just fill and return.
   //     (*insert_at) = new entry<T>(value);
   //     end  = root;
   //     return;
   //   } else {
   //      // Not first, need to traverse!
   //      if (value < insert_at->value) {
   //   }
#endif
