#include "stdio.h"
#include "string.h"
#include "stdlib.h"
#include "stdarg.h"

// Implementingmy own printf:

double average(int count, ...)
{
    printf ("Number of variabls:%i \n", count);
    va_list ap;
    int j;
    double tot = 0;
    va_start(ap, count); //Requires the last fixed parameter (to get the address)
    for(j=0; j<count; j++)
        tot+=va_arg(ap, double); //Requires the type to cast to. Increments ap to the next argument.
    va_end(ap);
    return tot/count;
}
char* convert (int in, char buf[256], int base) {
  char* chars  = "0123456789abcdef";
  char* ptr = buf + 200;
  *ptr = '\0';

  while (in != 0) {
    ptr--;
   *ptr = chars[in%base];
    in = in/base;
  }
  
  return ptr;
}
void my(char* fmt, ...) {
  // First travers the fmt until you reach a placeholder (%)
  char* traverse = fmt;

  va_list args;
  va_start(args,fmt);
  char * s;
  char buf[256] = {0};
  int nn;
  while ( *traverse != '\0') {
    while ( *traverse != '%' && *traverse != '\0' ) {
      putchar(*traverse++);
    }
    // if we reached a null terminate.
    if ( *traverse == '\0') {
      break;
    }

    // Print the place holder
    // Advance ot learn what the placeholder is:
    traverse++;
    switch (*traverse) {
      case 'S':
      case 's':
        s = va_arg (args, char *);
        fputs (s,stdout);
        break;
      case 'i':
        nn = va_arg(args, int);
        s = convert(nn, buf, 10);
        fputs (s, stdout);
        break;
      default:
        fputs ("not implemted yetm come back later", stdout);
    }
    traverse++; 
  }
  va_end(args);

}



int main() {

  // Need to figure out of this machine uses little or big indian.
  int x = 0x12345678;
  char buf[4]; // 4 bytes.
  
  // now copy the integer to the buf
  memcpy(buf, &x, sizeof(int));
  
  // now inspect the entries of the buffer
  // if the least significant byte went in the least sigificant address them it is little endian
  if (buf[0] == 0x78) {
    printf ("Little endianan!!!!\n");
  } 
  if (buf[0] == 0x12) {
    printf ("Big endianan!!!!\n");
  } 

 // printf ("No idea wjat I am doing here \n" ); 
 // printf ("Average is: %f\n", average(3,888, 7.9, 8.8));
  my ("Sameer = %s and is %i long\n", "is very good", 787);
  return 0;
}



