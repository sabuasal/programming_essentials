#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <map>
using namespace std;

long int N, M;
map<pair<long int, long int>, long int> sol_map;
vector<long int> coins;


long int num_sols(long int sum, long int m) {
    if (sum == 0)
        return 1;

    if (sum < 0)
      return 0;

    if (m <= 0) {
      return 0;
    }
// check if cached!!
    auto res = sol_map.find( pair<long int,long int>(sum, m) );
    if (res == sol_map.end() ) {
      // nit found 
    } else {
      return res->second;
    }

    long int tt =  num_sols(sum - coins[m - 1],  m) + num_sols(sum, m - 1);
    sol_map[pair<long int, long int>(sum, m)] = tt;
    return tt;
}

long int num_sols_driver(long int n) {
   long int total =0; 
   //now solve
   for (long int idx = n/2; idx >= 1 ; idx--) {
     //  total += num_sols(n-idx)*num_sols(idx);
    }
   // save the solution
   return total;
}

int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */ 
    long int tmp;
    cin >> N; // read the N
    cin >> M; // read the number if coins.
    for (int idx = 0; idx < M; idx++) {
      cin >> tmp;
      coins.push_back(tmp);
    }
   
    long int ss = num_sols(N, M);
    cout << ss << endl;
    return 0;
}

