#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <map>
 
using namespace std;


int M, N; 
vector <vector <bool> > land(M); // M rows, N columns.
int a;
bool is_legal (int x1, int x2, int y1, int y2) {
 return (land[x1][y1] & land[x2][y2] & land[x1][y2] & land[x2][y1]);
}

int area (int x1, int x2, int y1, int y2) {
  return 2 * (abs(x2 - x1) -1) + 2 * (abs(y2 - y1) - 1);
}



int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */   
    // M, N then M lines of length N
    cin >> M;
    cin >> N;
    char c;
    for (int row = 0; row < M; row++ ) {
        for (int col = 0; col < M; col++ ) {
            cin >> c;
            if (c == 'x')
              land[row].push_back(false);
            else
              land[row].push_back(true);
        }
    }

   pair<int, int>        up_left(0,0);
   pair<int, int> low_right(M-1, N-1);

   for (int x1 = 0; x1 < N; x1++) {
     for (int y1 = 0; y1 < M; y1++) { 
        for (int x2 = 0; x2 < N; x2++) {
          for (int y2  = 0; y2 < M; y2++) {
            if (is_legal(x1, x2, y1, y2))
              a = area(x1,x2,y1,y2) > a ? area(x1,x2,y1,y2) : a;
               
	}
	}
	}
	}
           
   
    if (a == -1)
      cout << "impossible" << endl;
    else
       cout << a << endl;
  
  return 0;
}

