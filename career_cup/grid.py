#!/usr/local/bin/python
import pdb

def search(G, g, R, C, r, c):
  #find the first line of r in R, then the folowin lines must all follow.
#  print "len(G) = ", len(G), "R = ", R
  for search_G in range(0, R - r + 1):
    #print search_G
    search_g = 0
    idx = G[search_G].find(g[search_g])
    #print "searching:", G[search_G], " for", g[search_g]
    #print "\t",idx
    while (idx > -1):
      for search_g_in in range(1, r):
        #print "matching inner: ", G[search_G + search_g_in][idx:], " with: ", g[search_g_in]
        match = (G[search_G + search_g_in][idx:]).startswith(g[search_g_in])  
        if match:
          if (search_g_in == r - 1):
            return "YES"
          continue
        else:
          break
      #print "searching:", G[search_G][idx + 1:], " for ", g[search_g]
      idx_new = G[search_G][idx + 1:].find(g[search_g])
      if (idx_new == -1):
        idx = -1
      else:
        idx = idx + 1  + idx_new 
      #print "New_idx:", idx

  return "NO"
  

def read_input():
  R,C = raw_input().strip().split(' ')
  R,C = [int(R),int(C)]
  G = []
  G_i = 0
  for G_i in xrange(R):
     Gt = str(raw_input().strip())
     G.append(Gt)

  r,c = raw_input().strip().split(' ')
  r,c = [int(r),int(c)]
  g = []
  P_i = 0
  for P_i in xrange(r):
     gt =  str(raw_input().strip())
     g.append(gt)

  return G, g, R, C, r, c


def main():
  t = int(raw_input().strip())
  for a0 in xrange(t):
    G, g, R,C, r, c =   read_input()
    print search(G, g, R, C, r, c)


if __name__ == "__main__":
    main()

