#include <iostream>
#include <string>

using namespace std; 

int main() {
  string input = "Hi,+++I am Sameer";
  string rep_org    = "+++";
  string rep_with   = "999";
  auto pos = input.find(rep_org);

  if (rep_with.length() != rep_org.length()) {
    cout << "Lengths are NOT equal" << endl;
    while (pos != string::npos) {
      // requires memory destrucion and creation
      input = input.substr(0, pos) + rep_with + input.substr(pos + rep_org.length(), input.length());
      pos = input.find(rep_org, pos + rep_org.length());
      cout <<"New string:" << input << endl <<"Match found at position: " << pos << endl;
    }
  } else {
    // Simply replace the old characters with new ones
    cout << "Lengths are equal" << endl;
    while (pos != string::npos) {
      input.replace(pos, rep_with.length(), rep_with);
      pos = input.find(rep_org, pos + rep_org.length());
      cout <<"New string:" << input << endl <<"Match found at position: " << pos << endl;
    }
  }

  cout << " After relacing stuff: " << input << endl;
  return 0;
}
