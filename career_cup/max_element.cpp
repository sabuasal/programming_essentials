#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;


int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */   
    int actions;
    int op;
    int input;
    int val;
    int max_val;
    int del_idx;
    int top_val;
    vector <int> vals;
    vector <int> max_vals;
    cin >> actions;
    for (int action =0; action < actions; action++) {
        cin >> op;
        switch (op) {
          case 1:  // Push to stack
            cin >> val;
            //cout << "Inserting: " << val << endl;
            //cout << "\tBefore insertion: max_vals.size() = " <<  max_vals.size() << endl;
            vals.push_back(val);
            if (max_vals.size() == 0) {
                max_vals.push_back(0);
                max_val = val;
            } else {
                if (val >= max_val) {
                    max_val = val;
                    max_vals.push_back(vals.size() - 1);
                }
            }
            //cout << "\tAfter insertion: max_vals.size() = " <<  max_vals.size() << endl;
            break;
          case 2: // Delete from the top of the stack.
            del_idx = vals.size() - 1;
            //cout << "del_idx = " << del_idx << endl;
            //cout << "\tbefore deletion: vals.size() = " << vals.size() << endl;
            top_val = vals[del_idx];
            vals.erase(vals.begin() + del_idx);
            //cout << "\tAfter deletion: vals.size() = " << vals.size() << endl; 
            //cout << "Deleting: " << top_val << endl;
            if (top_val == max_val) {
                // max_vals.erase(max_vals.begin() + max_vals.max_size() - 1);
                //cout << "\tBefore Deletion: max_vals.size() = " <<  max_vals.size() << endl;
                max_vals.erase(max_vals.begin() + max_vals.size() - 1);
                //cout << "\tAfter deletuion max_vals.size() = " <<  max_vals.size() << endl;
                if (max_vals.size() > 0) {
                  max_val = vals[max_vals[max_vals.size()-1]];
                  //cout << "Updating Max to: " << max_val << endl;
                } else {
                  //cout << "No max now!!!" << endl;
                }
            }      
            break;
          case 3:
            cout << max_val << endl;
        }
    }
    
    return 0;
}

