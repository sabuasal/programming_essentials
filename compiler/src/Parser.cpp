#include "Parser.h"
#include <iostream>
using namespace std;

Parser::Parser(string name) {
  FileName = name;
  InputFile = ifstream(name);
  BackTokStr = "";
  BackTok = tok_nomatch;
}

TokType Parser::matchTok(string Tok) {
  if (Tok.size() == 1)
    return matchOneChar(Tok[0]);
 return matchMultiChar(Tok);
}

TokType Parser::matchOneChar(char Tok) {
  switch (Tok) {
    case '(':
      return tok_open_paran;
    case ')':
      return tok_close_paran;
    case '{':
      return tok_open_brace;
    case '}':
      return tok_close_brace;
    case '+':
      return tok_plus;
    case '*':
      return tok_mul;
    case '-':
      return tok_minus;
    default:
      return tok_nomatch;
  }
}

TokType Parser::matchMultiChar(string tok) {
  if (tok == "def")
    return tok_def;
  if (tok == "extern")
    return tok_extern;
  return tok_id;
}

TokType Parser::getTok(string& Tok) {
  if (BackTokStr != "") {
    Tok = BackTokStr;
    BackTokStr = "";
    return BackTok;
  }
  Tok = "";
  char LastChar = ' ';
  while (InputFile >> noskipws >> LastChar) {
    if (LastChar == ' ' || LastChar == '\n' || LastChar == '\t') {
      if (Tok == "")
        continue;
      else
        return matchTok(Tok);
    }
    // Check if this last character matches one of the one char
    // operators (,),*,+.
    TokType OneCharTok = matchOneChar(LastChar);
    if (OneCharTok == tok_nomatch) {
      Tok += LastChar;
    } else {
      if (Tok == "") {
        Tok += LastChar;
        return matchTok(Tok);
      } else { // We went too far
        BackTok =  OneCharTok; // save the character we consumed.
        BackTokStr = LastChar;
        return matchTok(Tok);
      }
    }
  }
  if (Tok != "")
    return matchTok(Tok);
  return tok_eof;
}

int main(int argc, char** argv) {
  if (argc < 2) {
    cout << "Please pass file Name\n";
    return -1;
  }
  string TokStr;
  Parser p(argv[1]);
  while (tok_eof != p.getTok(TokStr))
    cout << "\tTokStr: " << TokStr << "\n";
  return 0;
}
