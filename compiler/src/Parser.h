#include <iostream>
#include <fstream>
#include <string>

using namespace std;


enum TokType {
  tok_eof,

  // Keywords
  tok_def,
  tok_extern,
  // Variables
  tok_id,
  tok_num,
  // Opertors (,+,-,*,), {,},;
  tok_open_brace,  // }
  tok_close_brace, // }
  tok_open_paran,  // (
  tok_close_paran, // )
  tok_plus, // +
  tok_minus,// -
  tok_mul,  // *
  tok_semi, // ;
  // No match
  tok_nomatch,
};

class Parser {
  public:
    Parser (string name);
    string FileName;
    ifstream InputFile;
    TokType getTok(string& tok);

  private:
    string  BackTokStr;
    TokType BackTok;

    TokType matchTok(string);
    TokType matchOneChar(char);
    TokType matchMultiChar(string);
};
